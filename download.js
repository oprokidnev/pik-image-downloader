const apis = require("./apis.json");

const https = require("https");
const fs = require("fs");
const { promisify } = require('util');
const fileExists = promisify(fs.exists);


(async (complexes) => {
  for await (let complex of complexes) {
    for (let downloadJob of complex) {
      await downloadJob();
    }
  }
})(
  (async function* () {
    for (const api of apis) {
      response = await new Promise((res) => {
        https.get(api.url, function (response) {
          let data = "";
          response.on("data", (c) => {
            data += c;
          });
          response.on("end", () => {
            res(data);
          });
        });
      });
      const data = JSON.parse(response);
      for (const year of data["years"]) {
        for (const month of year["months"]) {
          for (const dates of month["dates"]) {
            const files = dates["images"].map((e) => e.url);

            yield files.map((source, i) => {
              function basename(path, prefix) {
                path = path.split(prefix);
                return path[path.length - 1];
              }
              return async () => {
                const dirPath = `./files/${year['year']}-${month['month']}/${api.name}/`;
                fs.mkdirSync(dirPath, { recursive: true });
                const filePath = dirPath + basename(source, "/");
                const exists = await fileExists(filePath);
                console.log(filePath);
                if(exists){
                  return Promise.resolve();
                }
                const file = fs.createWriteStream(filePath);

                return new Promise((resolve, reject) => {
                  https.get("https:" + source, function (response) {
                    response.pipe(file);
                    response.on("end", () => {
                      resolve();
                    });
                  });
                });
              };
            });
          }
        }
      }
    }
  })()
);
